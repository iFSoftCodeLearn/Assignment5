﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A007.Excercise2 {
    public class CreateInvoiceCode {
        public static Dictionary<int, Dictionary<string, int>> YearInvoiceCount { get; private set; } = new Dictionary<int, Dictionary<string, int>>();

        public string GenerateInvoiceCode(string departmentCode, DateOnly invoiceDate) {
            int fYear = invoiceDate.Month >= 4 ? invoiceDate.Year : invoiceDate.Year - 1;
            int count = 1;
            if (YearInvoiceCount.TryGetValue(fYear, out var invoiceCount)) {
                if (invoiceCount.TryGetValue(departmentCode, out var c)) {
                    invoiceCount[departmentCode]++;
                    invoiceCount.TryGetValue(departmentCode, out count);
                }
                else {
                    invoiceCount.Add(departmentCode, 1);
                }
            }
            else {
                YearInvoiceCount.Add(fYear, new Dictionary<string, int> { { departmentCode, 1 } });
            }
            return departmentCode.ToUpper() + "FY" + fYear + count.ToString("D5");
        }

        public List<string> BatchGenerateCode (string departmentCode, DateOnly startDate, int invoicePerDay = 1, int numberOfDate = 1) {
            if (departmentCode.Length != 3) throw new ArgumentException("Department code must be 3");
            List<string> code = new List<string>();

            for (int x = 0; x < numberOfDate; x++) {
                DateOnly date = startDate.AddDays(x);
                for (int y = 0; y < invoicePerDay; y++) {
                    code.Add(GenerateInvoiceCode(departmentCode, date));
                }
            }

            return code;
        }
    }
}
