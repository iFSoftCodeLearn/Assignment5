﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A007.Excercise2 {
    public abstract class Check {
        public string CheckType { get => GetType().Name.Substring(5); }
        public virtual bool DoCheck(string? input, out string errorMessage) {
            errorMessage = string.Empty;
            if (string.IsNullOrEmpty(input)) {
                errorMessage = CheckType + " cannot be null.";
                return false;
            }
            return true;
        }
    }

    public class CheckDepName : Check {
        public override bool DoCheck(string? input, out string errorMessage) {
            if (!base.DoCheck(input, out errorMessage)) return false;
            input = input.Trim();

            foreach (var c in input) {
                if (!char.IsLetter(c)) {
                    errorMessage = CheckType + " must be letters";
                    return false;
                }
            }
            if (input.Length != 3) {
                errorMessage = CheckType + " must contains 3 lettes";
                return false;
            }
            return true;
        }
    }
    public class CheckDate : Check {

        public override bool DoCheck(string? input, out string errorMessage) {
            if (!base.DoCheck(input, out errorMessage)) return false;
            if (!DateTime.TryParseExact(input, Program.Format, Program.CultureInfo, Program.DateTimeStyles, out var value)) {
                string leadingWarning = "";
                if (Program.Format.Count(f => f == 'd') > 1) leadingWarning = " - leading zeroes are mandatory.";
                if (Program.Format.Count(f => f == 'M') > 1) leadingWarning = " - leading zeroes are mandatory.";
                errorMessage = $"{CheckType} is not in the correct format ({Program.Format})" + leadingWarning;
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }
    }
    public class CheckInt : Check {
        public override bool DoCheck(string? input, out string errorMessage) {
            if (!base.DoCheck(input, out errorMessage)) return false;
            if (int.TryParse(input, out int inputInt) && inputInt >= 0) {
                errorMessage = string.Empty;
                return true;
            }

            errorMessage = CheckType + " must be a number.";
            return false;
        }
    }
    public class CheckNumberOfInvoices : CheckInt { }
    public class CheckNumberOfDays : CheckInt { }
}
