﻿using System.Globalization;

namespace Net.M.A007.Excercise2 {
    internal class Program {
        public static string Format = "dd/MM/yyyy";
        public static CultureInfo CultureInfo = CultureInfo.InvariantCulture;
        public static DateTimeStyles DateTimeStyles = DateTimeStyles.None;

        static void Main(string[] args) {
            CreateInvoiceCode ic = new CreateInvoiceCode();
            while (true) {
                string dep = GetData(new CheckDepName());
                DateOnly date = DateOnly.ParseExact(GetData(new CheckDate()), Format, CultureInfo, DateTimeStyles);
                int inv = int.Parse(GetData(new CheckNumberOfInvoices()));
                int days = int.Parse(GetData(new CheckNumberOfDays()));
                Console.WriteLine();
                Console.WriteLine("Codes:");
                foreach (var i in ic.BatchGenerateCode(dep, date, inv, days)) Console.WriteLine(i);
                Console.WriteLine("\nPress any keys to generate more code OR ESC to end. . . ");
                ConsoleKeyInfo info = Console.ReadKey(true);
                if (info.Key == ConsoleKey.Escape) break;
            }
        }

        public static string GetData(Check check) {
            int count = 0;
            Console.WriteLine();
            while (true) {
                Console.Write($"Enter {check.CheckType}: ");
                string? get = Console.ReadLine();
                if (check.DoCheck(get, out var errorMessage)) {
                    ClearCurrentConsoleLine(2);
                    if (count > 0) {
                        ClearCurrentConsoleLine();
                    }
                    Console.WriteLine($"{check.CheckType}: {get}");
                    return string.Join(" ", get.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                }

                count++;
                ClearCurrentConsoleLine();
                if (count > 1) {
                    ClearCurrentConsoleLine();
                }
                Console.WriteLine(errorMessage);
            }
        }

        public static void ClearCurrentConsoleLine(int line = 1) {
            while (line > 0) {
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                int currentLineCursor = Console.CursorTop;
                Console.SetCursorPosition(0, Console.CursorTop);
                Console.Write(new string(' ', Console.BufferWidth));
                Console.SetCursorPosition(0, currentLineCursor);
                line--;
            }
        }
    }
}