﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A007.Excercise1.Models {
    internal class Invoices {
        public int ID { get; set; }
        public bool Approved { get; set; }
        public DateOnly CreatedDate { get; set; }
        public List<DateOnly> ReminderDates { get; private set; }
        public Invoices(bool approved = false) {
            Approved = approved;
            CreatedDate = DateOnly.FromDateTime(DateTime.Now);
            ReminderDates = Reminder.GetRemindingDate(CreatedDate);
        }

        public void SetReminder (DateOnly reminder) => ReminderDates.Add(reminder);
    }
}
