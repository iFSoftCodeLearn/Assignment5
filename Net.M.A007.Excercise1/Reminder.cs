﻿using Net.M.A007.Excercise1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A007.Excercise1 {
    internal static class Reminder {
        static DateOnly today = DateOnly.FromDateTime(DateTime.Today.AddDays(-1));
        public static TimeSpan alertTime = new TimeSpan(7, 0, 0);
        public static int FirstWait = 7;
        public static int ThenWait = 2;
        public static int MaxReminder = 5;

        static Reminder() {
            if (today != DateOnly.FromDateTime(DateTime.Today) && alertTime == DateTime.Now.TimeOfDay) {
                List<Invoices> RemindingInvoices = GetRemindingInvoices(DateOnly.FromDateTime(DateTime.Today));

                DoRemind(RemindingInvoices);
                today = DateOnly.FromDateTime(DateTime.Today);
            }
        }

        private static void DoRemind(List<Invoices> remindingInvoices) {
            foreach (Invoices i in remindingInvoices) {

            }
        }

        private static List<Invoices> GetRemindingInvoices (DateOnly today) {
            List<Invoices> ret = new List<Invoices>();
            foreach (var i in Program.Invoices) {
                if (!i.Approved && i.ReminderDates.Contains(today)) ret.Add(i); 
            }
            return ret;
        }

        public static List<DateOnly> GetRemindingDate (DateOnly createdDate) {
            List<DateOnly> remDate = new List<DateOnly>();

            remDate.Add(createdDate.AddDays(FirstWait));
            int n = MaxReminder - 1;
            while (n-- > 0) {
                remDate.Add(remDate[remDate.Count - 1].AddDays(ThenWait));
            }
            return remDate;
        }
    }
}
