﻿using Net.M.A007.Excercise1.Models;
using System;
using System.Globalization;
using System.Text;

namespace Net.M.A007.Excercise1 {
    internal class Program {
        public static List<Invoices> Invoices = new List<Invoices>();
        static void Main(string[] args) {
            DateOnly date = new DateOnly();
            while (true) {
                Console.Write("Enter date: ");
                string get = Console.ReadLine();
                if (DateOnly.TryParseExact(get, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var result)) {
                    date = result; break;
                }


                Console.Clear();
                Console.WriteLine("\x1b[3J");
                Console.Clear();
            }
            Console.WriteLine($"Enter date is {date.ToString("dd/MM/yyyy")}, Print out the day:");
            int n = 1;
            foreach (var i in Reminder.GetRemindingDate(date)) Console.WriteLine(n + AddOrdinal(n++) + " reminder: " + i.ToString("dd/MM/yyyy"));
            
            string AddOrdinal(int n) {
                int a = n % 10;
                if (n / 10 == 1) return "th";

                if (a == 1) return "st";
                else if (a == 2) return "nd";
                else if (a == 3) return "rd";

                return "th";
            }
        }
    }
}