﻿using System.Globalization;
using System.Text;

namespace Net.M.A007.Excercise3 {
    internal class Program {
        static void Main(string[] args) {
            List<string> block = ReadBlock();
            List<LineTimeInfo> lines = new List<LineTimeInfo>();
            foreach (string line in block) {
                if (!TryGetDateInfo(line, out DateTime datetime)) continue;
                lines.Add(new LineTimeInfo() { Line = line, DateTime = datetime }); 
            }
            if (lines.Count == 0) Console.WriteLine("No lines has datetime.");
            foreach (var line in lines.OrderBy(l => l.DateTime).Select(l => l.Line)) Console.WriteLine(line);
        }

        private static bool TryGetDateInfo(string line, out DateTime datetime) {
            datetime = DateTime.MinValue;
            
            string[] arr = line.Split(' ');
            for (int i = 0; i < arr.Length; i++) {
                if (DateTime.TryParseExact(arr[i], "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime)) {
                    if (arr.Length > i+1 && TimeOnly.TryParse(arr[i+1], CultureInfo.InvariantCulture, DateTimeStyles.None, out var time)) {
                        if (arr.Length > i + 2 && arr[i + 2].ToLower().Equals("pm") && time.Hour <= 12) {
                            datetime = datetime.AddHours(12);
                        }
                        datetime = datetime.AddTicks(time.Ticks);
                    }
                    return true;
                }
            }
            return false;
        }

        private static List<string> ReadBlock() {
            var result = new List<string>();
            StringBuilder sb = new StringBuilder();
            while (true) {
                ConsoleKeyInfo input = Console.ReadKey(true);
                if (input.Modifiers.HasFlag(ConsoleModifiers.Control) && input.Key.HasFlag(ConsoleKey.Enter)) {
                    result.Add(sb.ToString());
                    Console.WriteLine();
                    Console.WriteLine();
                    break;
                }
                else if (input.Key == ConsoleKey.Enter) {
                    Console.WriteLine();
                    result.Add(sb.ToString());
                    sb.Clear();
                }
                else if (input.Key == ConsoleKey.Backspace) {
                    if (sb.Length == 0) {
                        if (result.Count != 0) {
                            sb.Append(result[result.Count - 1]);
                            result.RemoveAt(result.Count - 1);
                            Console.SetCursorPosition(0, Console.CursorTop - 1);
                            Console.Write(sb.ToString());
                        }
                    }
                    else {
                        sb.Remove(sb.Length - 1, 1);
                        //Console.SetCursorPosition(0, Console.CursorTop);
                        Console.Write("\b \b");
                        //Console.Write(sb);
                    }
                    //Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
                }
                else {
                    sb.Append(input.KeyChar);
                    Console.Write(input.KeyChar);
                }
            }
            return result;
        }
    }
}