﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A007.Excercise3 {
    internal class LineTimeInfo {
        public string Line { get; set; }
        public DateTime DateTime { get; set; }
    }
}
